#!/bin/sh

set -e
set -u

EXTENSION=drps_arrows
SRC=src

# Set working directory to location of this script
DIR=$(dirname $(readlink -m "$0"))
cd "$DIR"

mkdir -p releases

VERSION=$(grep '"version":' "$SRC"/manifest.json | sed 's/.*"\([0-9.]*\)".*/\1/')
NAME_VER="$EXTENSION"-"$VERSION"
CHROME_ZIP=releases/"$NAME_VER".zip
FIREFOX_XPI=releases/"$NAME_VER".xpi

[ -e "$SRC"/icons/green_arrows_16.png ] || ./make_icons.sh

# Generic zipping:
rm -f "$CHROME_ZIP"
cd "$SRC"
zip -q -r -FS ../"$CHROME_ZIP" *
cd ..
echo Created zip for upload to Chrome store: "$CHROME_ZIP"

# Firefox version for self-hosting:
#
# Change the UUID if you fork this extension! For example, in Python:
# import uuid; print(str(uuid.uuid4()))
FIREFOX_UUID=75191f08-a5e3-46d5-881b-b9f66fe78fc5
rm -f "$FIREFOX_XPI"
TMP_DIR=$(mktemp -d -p .)
CLEAR_IT () {
    cd "$DIR"
    rm -r "$TMP_DIR"
}
trap CLEAR_IT EXIT
cp -rip "$SRC" "$TMP_DIR"
cd "$TMP_DIR/$SRC"
perl -e 's/_[0-9]*\.png/.svg/gi' -p -i *.js *.html *.json
rm icons/*.png
sed -e :a -e '$d;N;2,3ba' -e 'P;D' manifest.json > manifest_firefox.json
cat >> manifest_firefox.json <<EOF
  },

  "browser_specific_settings": {
    "gecko": {
EOF
echo '      "id": "{'"$FIREFOX_UUID"'}",' >> manifest_firefox.json
cat >> manifest_firefox.json <<EOF
      "update_url": "https://homepages.inf.ed.ac.uk/imurray2/pt/drps_arrows/firefox_updates.json"
    }
  }

}
EOF
mv manifest_firefox.json manifest.json
zip -q -r -FS ../../"$FIREFOX_XPI" *
cd ../..
echo Created Firefox .xpi for self-hosting: "$FIREFOX_XPI"
if [ \! -e releases/firefox_updates.json ] ; then
cat > releases/firefox_updates.json <<EOF
{
  "addons": {
EOF
echo '    "{'"$FIREFOX_UUID"'}": {' >> releases/firefox_updates.json
echo '      "updates": [' >> releases/firefox_updates.json
echo '        { "version": "'"$VERSION"'",' >> releases/firefox_updates.json
echo '          "update_link": "https://homepages.inf.ed.ac.uk/imurray2/pt/drps_arrows/archive/drps_arrows-'"$VERSION"'-an+fx.xpi"' >> releases/firefox_updates.json
cat >> releases/firefox_updates.json <<EOF
        }
      ]
    }
  }
}
EOF
echo "Created new releases/firefox_updates.json"
else
if grep "$VERSION" releases/firefox_updates.json > /dev/null ; then
echo "This release already in releases/firefox_updates.json"
else
sed -e :a -e '$d;N;2,5ba' -e 'P;D' releases/firefox_updates.json > releases/firefox_updates.json.tmp
echo '        },' >> releases/firefox_updates.json.tmp
echo '        { "version": "'"$VERSION"'",' >> releases/firefox_updates.json.tmp
echo '          "update_link": "https://homepages.inf.ed.ac.uk/imurray2/pt/drps_arrows/archive/drps_arrows-'"$VERSION"'-an+fx.xpi"' >> releases/firefox_updates.json.tmp
cat >> releases/firefox_updates.json.tmp <<EOF
        }
      ]
    }
  }
}
EOF
mv releases/firefox_updates.json.tmp releases/firefox_updates.json
echo "Added to releases/firefox_updates.json"
fi
fi


# Create user script
USERJS=releases/"$NAME_VER".user.js

(cat <<EOF
// ==UserScript==
// @name         DRPS Arrows
// @namespace    https://homepages.inf.ed.ac.uk/imurray2/pt/drps_arrows/
EOF
echo '// @version      '"$VERSION" ; \
cat <<EOF
// @description  Navigate DRPS sessions with ctrl-left, ctrl-right, ctrl-up. Also add links to course codes in DRPS plain text.
// @author       Iain Murray
// @match        *://www.drps.ed.ac.uk/*
// @grant        none
// ==/UserScript==

(function() {
EOF
    cat "$SRC"/session.js ; \
    sed -n '/session\.js/{ :a; n; p; ba; }' "$SRC"/content.js ; \
    echo '})();' ) > "$USERJS"
echo Created "$USERJS"

