"use strict";

// Unless I catch errors, chrome gives error messages when restarting the
// extension, as it doesn't remove old versions of the content script.
let listeners = [];
function killListeners() {
  for (const [e, action] of listeners) {
    window.removeEventListener(e, action);
  }
  listeners = [];
}

const hideMessage = () => { try { chrome.runtime.sendMessage({source: 'content_hide'}); } catch { killListeners(); } };
const showMessage = () => { try { chrome.runtime.sendMessage({source: 'content_show'}); } catch { killListeners(); } };

// The timeout pushes the messages to the end of the queue, so that on extension
// creation the background script has a chance to start up. In Firefox, the
// right page actions then appear. In Chrome the content scripts aren't loaded
// unless the user manually refreshes.
setTimeout(() => {
  listeners.push(['pagehide', hideMessage]);
  listeners.push(['pageshow', showMessage]);
  for (const [e, action] of listeners) {
    window.addEventListener(e, action);
  }
  chrome.runtime.sendMessage({source: 'content_show'});
}, 0);
