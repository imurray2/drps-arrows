"use strict";

// Everything beneath here could be in a Userscript (for Greasemonkey, TamperMonkey, etc.)
// ...if concatenate to end of session.js

let URL = location.href;
const match = URL.match(/[0-9]{2}-[0-9]{2}(?=\/)/);
let SESSION = match && match[0];

// A hack for when /current/ in URL (a new DRPS feature, since first writing this extension).
if (!SESSION && (URL.search('/current/') >= 0)) {
  for (const el of [... document.getElementsByTagName('link')]) {
    const elMatch = el.href.match(/[0-9]{2}-[0-9]{2}(?=\/)/);
    if (elMatch) {
      SESSION = elMatch[0];
      URL = URL.replace('current', SESSION);
      break
    }
  }
}

if (SESSION) {

  const y1 = Number(SESSION.slice(0, 2));
  const prev = sessionString(y1 - 1);
  const next = sessionString(y1 + 1);

  // Navigate sessions with ctrl-left and ctrl-right
  for (const [key, newSession] of [['Right', next], ['Left', prev], ['Up', 'current']]) {
    document.addEventListener('keyup', e => {
      if ((e.code == ('Arrow' + key)) && (e.ctrlKey || e.metaKey)) {
        location.href = URL.replace(SESSION, newSession);
      }
    });
  }

  // Add navigation links to year in title (if present)
  let h1 = document.getElementsByTagName('h1');
  h1 = h1 && h1[0];
  let canary = document.getElementById('drps_arrows_touched');
  if (h1 && !canary) {
    const year_re = /(20[0-9][0-9].20[0-9][0-9])/;
    const year_match = h1.textContent.match(year_re);
    if (year_match) {
      const year_str = year_match[0];
      h1.textContent = h1.textContent.replace(year_re, '');
      //
      let leftNode = document.createElement('a');
      leftNode.textContent = '⟵';
      leftNode.style.textDecoration = 'none';
      leftNode.style.color = 'white';
      leftNode.style.fontWeight = 'bold';
      leftNode.href = URL.replace(SESSION, prev);
      leftNode.title = 'Previous session, Ctrl+left';
      h1.appendChild(leftNode);
      //
      let pageY1 = Number(SESSION.slice(0, 2));
      let nowY1 = Number(thisSession.slice(0, 2));
      let yearNode;
      if (pageY1 < nowY1) {
        yearNode = document.createElement('a');
        yearNode.href = URL.replace(SESSION, 'current');
        yearNode.title = 'Go to current session, Ctrl+up';
        yearNode.style.color = 'rgb(255, 35, 35)';
      } else if (pageY1 > nowY1) {
        yearNode = document.createElement('a');
        yearNode.href = URL.replace(SESSION, 'current');
        yearNode.title = 'Go to current session, Ctrl+up';
        yearNode.style.color = 'rgb(35, 200, 35)';
      } else {
        yearNode = document.createElement('span');
      }
      yearNode.textContent = year_str;
      yearNode.id = 'drps_arrows_touched';
      h1.appendChild(yearNode);
      //
      let rightNode = document.createElement('a');
      rightNode.textContent = '⟶';
      rightNode.style.textDecoration = 'none';
      rightNode.style.color = 'white';
      rightNode.style.fontWeight = 'bold';
      rightNode.href = URL.replace(SESSION, next);
      rightNode.title = 'Next session, Ctrl+right';
      h1.appendChild(rightNode);
    }
  }
}


// Add hyperlinks to course codes and unformatted URLs.

function addLinks(node, regex, urlFn) {
  for (let curNode of [... node.childNodes]) {
    if (curNode.nodeType == Node.ELEMENT_NODE) {
      const skip = ['a', 'style', 'script', 'h1'];
      const tag = curNode.tagName.toLowerCase();
      if (skip.indexOf(tag) == -1) {
        addLinks(curNode, regex, urlFn);
      }
    } else if (curNode.nodeType == Node.TEXT_NODE) {
      let match;
      while (match = curNode.textContent.match(regex)) { // not ==
        if (match.index == undefined) {
          console.log('Problem with matching. Maybe used g in regex?');
          return;
        }
        const nextNode = curNode.splitText(match.index + match[0].length);
        curNode.textContent = curNode.textContent.slice(0, match.index);
        let linkNode = document.createElement('a');
        linkNode.href = urlFn(match[0]);
        linkNode.textContent = match[0];
        node.insertBefore(linkNode, nextNode);
        curNode = nextNode;
      }
    }
  }
}

// There's a trade-off with last character in this regex. Will miss some URLs,
// but will spuriously pull in punctuation and formatting less often.
if (SESSION) {
  const urlRegex = /(https?:\/\/|www\.)[^ \n]+[a-zA-Z0-9/]/;
  addLinks(document.body, urlRegex, x => (x.slice(0, 4) == 'http') ? x : 'http://' + x);
  function courseLink(code) {
    return 'http://www.drps.ed.ac.uk/' + SESSION + '/dpt/cx' + code.toLowerCase() + '.htm';
  }
  const courseRegex = /([a-zA-Z]{4}[01][0-9]{4})(?=([^0-9]|$))/;
  addLinks(document.body, courseRegex, courseLink);
}
