"use strict";

chrome.runtime.sendMessage({source: 'menu_init'}, sessions => {
  if (sessions.cur) {
    document.getElementById('cur').firstChild.textContent = 'Current session'
  } else {
    document.getElementById('cur_block').hidden = true;
  }
  document.getElementById('prev').firstChild.textContent = '← ' + sessions.prev;
  document.getElementById('next').firstChild.textContent = '→ ' + sessions.next;
});

document.addEventListener('click', event => {
  let id = event.target.id || event.target.parentNode.id;
  if (id != "") {
    chrome.runtime.sendMessage({source: 'menu', menu: id}, () => window.close());
  }
});

