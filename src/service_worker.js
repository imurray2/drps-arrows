importScripts("session.js");

"use strict";

let sessionsRegex = /(\/)([0-9][0-9])(-)([0-9][0-9])(\/)/;

/* After the swap to manifest v3, Chrome apparently stopped allowing .svg in setIcon calls?
 * So the code specifies a .png for each resolution for Chrome. When packaging the Firefox version,
 * I replace mutliple .png's with a single .svg, but otherwise leave the code the same for ease of maintenance. */
GRAY_ARROWS = {
  16: "icons/gray_arrows_16.png",
  19: "icons/gray_arrows_19.png",
  38: "icons/gray_arrows_38.png",
  48: "icons/gray_arrows_48.png",
  96: "icons/gray_arrows_96.png",
  128: "icons/gray_arrows_128.png",
  512: "icons/gray_arrows_512.png"
};
RED_ARROWS = {
  16: "icons/red_arrows_16.png",
  19: "icons/red_arrows_19.png",
  38: "icons/red_arrows_38.png",
  48: "icons/red_arrows_48.png",
  96: "icons/red_arrows_96.png",
  128: "icons/red_arrows_128.png",
  512: "icons/red_arrows_512.png"
};
GREEN_ARROWS = {
  16: "icons/green_arrows_16.png",
  19: "icons/green_arrows_19.png",
  38: "icons/green_arrows_38.png",
  48: "icons/green_arrows_48.png",
  96: "icons/green_arrows_96.png",
  128: "icons/green_arrows_128.png",
  512: "icons/green_arrows_512.png"
};

function handleContentHide(request, sender, sendResponse) {
  chrome.action.setIcon({tabId: sender.tab.id, path: GRAY_ARROWS});
  chrome.action.disable(sender.tab.id);
}


function handleContentShow(request, sender, sendResponse) {
  if ((sender.url.search(thisSession) >= 0) || (sender.url.search('current') >= 0)) {
    chrome.action.setIcon({tabId: sender.tab.id, path: GRAY_ARROWS});
    chrome.action.enable(sender.tab.id);
  } else if (sender.url.match(sessionsRegex)) {
    if (Number(sender.url.match(sessionsRegex)[0].slice(1, 3)) < Number(thisSession.slice(0, 2))) {
      chrome.action.setIcon({tabId: sender.tab.id, path: RED_ARROWS});
    } else {
      chrome.action.setIcon({tabId: sender.tab.id, path: GREEN_ARROWS});
    }
    chrome.action.enable(sender.tab.id);
  } else {
    handleContentHide(request, sender, sendResponse);
  }
}


let SESSIONS;
let BASE_URL;
let TAB;
function handleMenuInit(request, sender, sendResponse) {
  let cur;
  let prev;
  let next;
  chrome.tabs.query({active: true, currentWindow: true}, tabs => {
    TAB = tabs[0];
    BASE_URL = TAB && TAB.url;
    if (BASE_URL) {
      if ((BASE_URL.search(thisSession) >= 0) || (BASE_URL.search('current') >= 0)) {
        cur = undefined;
        prev = sessionString(curStartYear - 1);
        next = sessionString(curStartYear + 1);
      } else {
        let match = BASE_URL.match(sessionsRegex);
        cur = 'current';
        let startYear = Number(match[2]);
        prev = sessionString(startYear - 1);
        next = sessionString(startYear + 1);
      }
      SESSIONS = {cur: cur, prev: prev, next: next};
      sendResponse(SESSIONS);
    }
  });
}


function handleMenu(request, sender, sendResponse) {
  let newURL;
  if (BASE_URL.search('current') >= 0) {
    newURL = BASE_URL.replace('current', SESSIONS[request.menu]);
  } else {
    newURL = BASE_URL.replace(sessionsRegex, '$1' + SESSIONS[request.menu] + '$5');
  }
  chrome.tabs.update(TAB.id, {url: newURL});
  sendResponse(true);
}


function handleMessage(request, sender, sendResponse) {
  const dispatch = {
    'src_content_hide': handleContentHide,
    'src_content_show': handleContentShow,
    'src_menu_init': handleMenuInit,
    'src_menu': handleMenu
  };
  let key = "src_" + request.source;
  if (key in dispatch) {
    dispatch[key](request, sender, sendResponse);
  } else {
    console.log("service_worker.js didn't recognize request", request);
  }
  return true;
};
chrome.runtime.onMessage.addListener(handleMessage);
