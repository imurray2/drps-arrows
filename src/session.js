"use strict";

let curDate = new Date();
let curYear = Number(String(curDate.getFullYear()).slice(-2));
let curStartYear;
// From June, start showing session that starts this year
if (curDate.getMonth() < 6) {
  curStartYear = curYear - 1;
} else {
  curStartYear = curYear;
}
function sessionString(y1) {
  function f(year) {
    return ("0" + year).slice(-2);
  }
  return f(y1) + '-' + f(y1 + 1);
}
let thisSession = sessionString(curStartYear);

