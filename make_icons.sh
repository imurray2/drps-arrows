#!/bin/sh

# Needs:
# https://www.imagemagick.org/
# https://www.gnu.org/software/parallel/  (or just delete the "| parallel")

set -e

for sz in 16 19 38 48 96 128 512 ; do
    for img in src/icons/*.svg ; do
        echo convert -density 600 -background none -geometry "$sz"x"$sz" "$img" "${img%.svg}"_"$sz".png
    done
done | parallel

